$:.unshift File.join(File.dirname(__FILE__), 'lib')
require 'pyrb/rspec/version'

Gem::Specification.new do |s|
  s.name     = 'pyrb-rspec'
  s.version  = ::Pyrb::RSpec::VERSION
  s.authors  = ['Cameron Dutro']
  s.email    = ['camertron@gmail.com']
  s.homepage = 'http://gitlab.com/camertron/pyrb-rspec'

  s.description = s.summary = 'Run your transpiled Python unit tests with RSpec.'

  s.platform = Gem::Platform::RUBY
  s.has_rdoc = true

  s.add_dependency 'pyrb-runtime', '~> 1.0'
  s.add_dependency 'rspec', '~> 3.0'

  s.require_path = 'lib'
  s.files = Dir['{lib,spec}/**/*', 'Gemfile', 'CHANGELOG.md', 'README.md', 'Rakefile', 'pyrb-rspec.gemspec']

  s.executables << 'pyrb-rspec'
end
