module Pyrb
  module RSpec
    AssertWarnsContext = Module.new do
      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['AssertWarnsContext'] = Pyrb::PythonClass.new('AssertWarnsContext', [Pyrb::Object], {
        '__init__' => Pyrb.defn(self: nil, expected: nil, msg: { default: nil }) do |self_, expected, msg|
          self_.attrs['expected'] = expected
          self_.attrs['msg'] = msg
        end,

        '__enter__' => Pyrb.defn(self: nil) do |self_|
          self_.attrs['warnings_manager'] = Pyrb::Warnings.exports['catch_warnings'].new(record: true)
          self_.attrs['warnings'] = self_.attrs['warnings_manager'].fcall('__enter__')
          self_.ivars['asserter'] = Asserter.new
        end,

        '__exit__' => -> (args = [], **kwargs) do
          self_, typ, value, traceback = args
          self_.attrs['warnings_manager'].fcall('__exit__')

          found = self_.attrs['warnings'].find do |msg|
            msg.attr('category') == self_.attrs['expected']
          end

          if found
            self_.attrs['warning'] = found.attr('message')
            self_.attrs['filename'] = found.attr('filename')
            self_.attrs['lineno'] = found.attr('lineno')
            return
          end

          message = self_.attrs['msg'] ||
            "Expected warning #{self_.attrs['expected'].attr('__name__')} to be triggered"

          self_.ivars['asserter'].assert_not_equal(
            found, nil, message
          )
        end
      })
    end.exports['AssertWarnsContext']
  end
end
