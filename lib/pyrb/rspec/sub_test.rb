module Pyrb
  module RSpec
    SubTest = Module.new do
      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['SubTest'] = Pyrb::PythonClass.new('SubTest', [Pyrb::Object], {
        '__init__' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          self_.attrs['test_case'] = args.get(1)
          self_.attrs['message'] = args.get(2)
          self_.attrs['params'] = args.get(3)
        end,

        '__around__' => -> (args = [], **kwargs) do
          self_ = args.get(0)

          RSpec.describe do
            ctx = []
            ctx << "[#{self_.attrs['message']}]" if self_.attrs['message']

            if params = self_.attrs['params']
              param_strs = params.map do |param_key, param_value|
                "#{param_key}=#{Pyrb.repr.call([param_value])}"
              end

              ctx << "(#{param_strs.join(', ')})"
            end

            context ctx do
              args.get(1).call  # call the block
            end
          end
        end
      })
    end.exports['SubTest']
  end
end
