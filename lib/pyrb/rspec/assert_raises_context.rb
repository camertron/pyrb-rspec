module Pyrb
  module RSpec
    AssertRaisesContext = Module.new do
      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['AssertRaisesContext'] = Pyrb::PythonClass.new('AssertRaisesContext', [Pyrb::Object], {
        '__init__' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          self_.attrs['exception'] = args.get(1)
          self_.attrs['msg'] = args.get(2)
          self_.ivars['asserter'] = Asserter.new
        end,

        '__enter__' => -> (args = [], **kwargs) do
          # pass
        end,

        '__exit__' => -> (args = [], **kwargs) do
          self_, typ, value, traceback = args

          self_.ivars['asserter'].assert_raises(
            self_.attr('exception'),
            -> (*) { raise(value) if value },
            [],
            {},
            self_.attr('msg')
          )

          true
        end
      })
    end.exports['AssertRaisesContext']
  end
end
