module Pyrb
  module RSpec
    TestCase = Module.new do
      extend Pyrb::Runtime
      using Pyrb::Runtime

      exports['TestCase'] = Pyrb::PythonClass.new('TestCase', [Pyrb::Object], {
        '__init__' => Pyrb.defn(self: nil, methodName: { default: 'runTest' }) do |self_, method_name|
          self_.attrs['_testMethodName'] = method_name
          self_.ivars['asserter'] = Asserter.new
        end,

        'subTest' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          msg = args.get(1)
          SubTest.new([self, msg, kwargs])
        end,

        'assertEqual' => -> (args = [], **kwargs) do
          self_, first, second = args
          self_.ivars['asserter'].assert_equal(first, second)
        end,

        'assertNotEqual' => -> (args = [], **kwargs) do
          self_, first, second = args
          self_.ivars['asserter'].assert_not_equal(first, second)
        end,

        'assertGreaterEqual' => -> (args = [], **kwargs) do
          self_, first, second = args
          self_.ivars['asserter'].assert_greater_equal(first, second)
        end,

        'assertCountEqual' => -> (args = [], **kwargs) do
          self_, first, second = args
          self_.ivars['asserter'].assert_count_equal(first, second)
        end,

        'assertIs' => -> (args = [], **kwargs) do
          self_, first, second = args
          self_.ivars['asserter'].assert_equal(first, second)
        end,

        'assertMultiLineEqual' => -> (args = [], **kwargs) do
          self_, first, second = args
          self_.ivars['asserter'].assert_equal(first, second)
        end,

        'assertTrue' => -> (args = [], **kwargs) do
          self_, value = args
          self_.ivars['asserter'].assert_true(value)
        end,

        'assertFalse' => -> (args = [], **kwargs) do
          self_, value = args
          self_.ivars['asserter'].assert_false(value)
        end,

        'assertIn' => -> (args = [], **kwargs) do
          self_, collection, item = args
          self_.ivars['asserter'].assert_in(collection, item)
        end,

        'assertRaises' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          exception = args.get(1)
          callable = args.get(2)
          msg = kwargs[:msg]

          if Pyrb.callable.call([callable])
            self_.ivars['asserter'].assert_raises(exception, callable, args.get(3..-1), kwargs, msg)
          else
            AssertRaisesContext.new([exception, msg])
          end
        end,

        'assertRaisesRegex' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          exception = args.get(1)
          regex = args.get(2)
          callable = args.get(3)
          msg = kwargs[:msg]

          if Pyrb.callable.call([callable])
            self_.ivars['asserter'].assert_raises_regex(exception, regex, callable, args.get(4..-1), kwargs, msg)
          else
            AssertRaisesRegexContext.new([exception, regex, msg])
          end
        end,

        'assertWarns' => -> (args = [], **kwargs) do
          self_ = args.get(0)
          warning = args.get(1)
          callable = args.get(2)
          msg = kwargs[:msg]

          context = AssertWarnsContext.new([warning, msg])

          if Pyrb.callable.call([callable])
            Pyrb.with.call([cm]) { callable.call }
          else
            context
          end
        end
      })
    end.exports['TestCase']
  end
end
