require 'rspec/core/rake_task'

module Pyrb
  module RSpec
    class RakeTask < ::RSpec::Core::RakeTask
      DEFAULT_RSPEC_PATH = File.expand_path('../../../bin/pyrb-rspec', __dir__).freeze
      DEFAULT_PATTERN = './spec/**/test_*.rb'.freeze

      def initialize(*args, &block)
        super
        @rspec_path = DEFAULT_RSPEC_PATH
        @pattern = DEFAULT_PATTERN
      end
    end
  end
end
