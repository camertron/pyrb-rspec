module Pyrb
  module RSpec
    using Pyrb::Runtime

    ::RSpec::Matchers::BuiltIn::Eq.class_eval do
      def match(expected, actual)
        # Forces the matcher to use Python equality semantics.
        # We can't use a refinement here because the matcher is evaluated
        # by RSpec internals which doesn't have the Pyrb::Runtime refinement
        # applied. The good news is that this overridden match function is
        # a 1:1 copy of the original, meaning that outside this module, Python
        # equality won't be in effect.
        actual == expected
      end
    end

    class IncludeShim < SimpleDelegator
      def include?(item)
        __getobj__.fcall('__contains__', [item])
      end
    end

    ::RSpec::Matchers::BuiltIn::Include.class_eval do
      def matches_with_shim?(actual)
        matches_without_shim?(IncludeShim.new(actual))
      end

      alias_method :matches_without_shim?, :matches?
      alias_method :matches?, :matches_with_shim?
    end

    class Asserter
      include ::RSpec::Matchers

      def assert_equal(first, second, msg = nil)
        if Pyrb.hasattr.call([first, '__next__'])
          first = Pyrb::List.call([first])
        end

        if Pyrb.hasattr.call([second, '__next__'])
          second = Pyrb::List.call([second])
        end

        expect(first).to eq(second), msg
      end

      def assert_not_equal(first, second, msg = nil)
        if Pyrb.hasattr.call([first, '__next__'])
          first = Pyrb::List.call([first])
        end

        if Pyrb.hasattr.call([second, '__next__'])
          second = Pyrb::List.call([second])
        end

        expect(first).to_not eq(second), msg
      end

      def assert_greater_equal(first, second, msg = nil)
        if Pyrb.hasattr.call([first, '__next__'])
          first = Pyrb::List.call([first])
        end

        if Pyrb.hasattr.call([second, '__next__'])
          second = Pyrb::List.call([second])
        end

        expect(first).to(be >= second, msg)
      end

      def assert_count_equal(first, second, msg = nil)
        expect(Pyrb.len.call([second])).to eq(Pyrb.len.call([first])), msg
      end

      def assert_true(value)
        expect(!!value).to eq(true)
      end

      def assert_false(value)
        expect(!!value).to eq(false)
      end

      def assert_in(item, collection)
        expect(collection).to include(item)
      end

      def assert_raises(exceptions, callable, args, kwargs, msg)
        exceptions = Array(exceptions).map { |ex| ex.is_a?(Pyrb::PythonClass) ? ex.klass : ex }
        expect { callable.call(args, **kwargs) }.to raise_error do |exception|
          expect(exceptions).to include(exception.class)
        end
      end

      def assert_raises_regex(exceptions, regex, callable, args, kwargs, msg)
        exceptions = Array(exceptions).map { |ex| ex.is_a?(Pyrb::PythonClass) ? ex.klass : ex }
        expect { callable.call(args, **kwargs) }.to raise_error do |exception|
          expect(exceptions).to include(exception.class)
          expect(exception.message).to match(regex)
        end
      end
    end
  end
end
