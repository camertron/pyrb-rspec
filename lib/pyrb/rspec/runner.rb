require 'pyrb'
require 'rspec/core'

# Unfortunately RSpec doesn't allow you to pass file_path or line_number in as
# example metadata, so instead the following module prepends are necessary. The
# idea here is to override the example's metadata with the location of the
# actual test method, which Pyrb's runtime keeps track of for us.
::RSpec::Core::Metadata::ExampleHash.prepend(Module.new do
  def populate_location_attributes
    super

    python_method = user_metadata[:python_method]
    return unless python_method

    file, line = python_method
      .attrs.get('__func__')
      .instance_variable_get(:@__pyrb_location__)
      .split(':')

    metadata.merge!(
      file_path: file,
      line_number: line.to_i,
      location: "#{file}:#{line}",
      absolute_file_path: file,
      rerun_file_path: file
    )
  end
end)

::RSpec::Core::Metadata::ExampleGroupHash.prepend(Module.new do
  def populate_location_attributes
    super

    python_class = user_metadata[:python_class]
    return unless python_class

    file, line = python_class
      .source_location
      .split(':')

    metadata.merge!(
      file_path: file,
      line_number: line.to_i,
      location: "#{file}:#{line}",
      absolute_file_path: file,
      rerun_file_path: file
    )
  end
end)

::RSpec::Core::Configuration.prepend(Module.new do
  def initialize(*args, &block)
    super

    # Default pattern that looks for test*.rb instead of *_spec.rb. Necessary
    # to enable running `bundle exec pyrb-rspec` without passing an explicit
    # file pattern.
    @pattern = '**{,/*/**}/test_*.rb'
  end

  # Ugh, no choice but to override this with the right command name. Doing so
  # enables running `bundle exec pyrb-rspec` without manually specifying a
  # spec directory.
  #
  # https://github.com/rspec/rspec-core/blob/v3.8.0/lib/rspec/core/configuration.rb#L1028
  def files_or_directories_to_run=(*files)
    files = files.flatten

    if (command == 'pyrb-rspec' || Runner.running_in_drb?) && default_path && files.empty?
      files << default_path
    end

    @files_or_directories_to_run = files
    @files_to_run = nil
  end
end)

module Pyrb
  module RSpec
    class Runner < ::RSpec::Core::Runner
      def setup(err, out)
        configure(err, out)

        # replace default TestCase implementation with RSpec one
        unittest, = Pyrb.import('unittest')
        unittest.exports['TestCase'] = Pyrb::RSpec::TestCase

        @configuration.files_to_run.each do |file|
          require file
          load_testcases_in(Pyrb::Sys.exports['modules'][file], file)
          @configuration.loaded_spec_files << file
        end

        @world.announce_filters
      end

      private

      def load_testcases_in(mod, file_path)
        test_cases = mod.exports.values.select do |export|
          export.is_a?(Pyrb::PythonClass) && export.parents.include?(Pyrb::RSpec::TestCase)
        end

        test_cases.each do |test_case|
          ::RSpec.describe(test_case.class_name, python_class: test_case) do
            instance = test_case.new

            instance.attr('__dict__').fcall('__iter__').each do |name|
              if name.start_with?('test_')
                it(name.sub(/\Atest_/, '').gsub('_', ' '), python_method: instance.attr(name)) do
                  if Pyrb.hasattr.call([instance, 'setUp'])
                    instance.fcall('setUp')
                  end

                  instance.fcall(name)

                  if Pyrb.hasattr.call([instance, 'tearDown'])
                    instance.fcall('tearDown')
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
